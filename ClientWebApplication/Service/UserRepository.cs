﻿using System.Collections.Generic;
using System.Linq;
using ClientWebApplication.UserServiceReference;

namespace ClientWebApplication.Service
{
    public class UserRepository : IUserRepository
    {
        private readonly UserServiceClient _userServiceClient = new UserServiceClient();
        public AuthDto IsAuthenticate(string userName, string password)
        {
            return _userServiceClient.Authenticate(userName, password);
        }

        public bool ChangePassword(int id, string oldPassword, string newPassword)
        {
            return _userServiceClient.ChangePassword(id, oldPassword, newPassword);
        }

        public UserDto SignIn(string userName, string password, string email)
        {
           var user = _userServiceClient.AddUser(userName, password, email);
           return new UserDto
           {
               Id = user.Id,
               Name = user.UserName,
               Email = user.Email
           };
        }

        public List<UserDto> GetAllUsers()
        {
            var users = _userServiceClient.GetAllUsers();

            return UsersToDto(users);
        }

        private List<UserDto> UsersToDto(IEnumerable<User> users)
        {
            var userDtos = new List<UserDto>(users.Count());

            userDtos.AddRange(users.Select(user => new UserDto
            {
                Name = user.UserName, Id = user.Id, Email = user.Email
            }));

            return userDtos;
        }
    }
}