﻿namespace ClientWebApplication.Service
{
    public class UserDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
    }
}