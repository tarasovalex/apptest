﻿using System.Collections.Generic;
using ClientWebApplication.UserServiceReference;

namespace ClientWebApplication.Service
{
    public interface IUserRepository
    {
        AuthDto IsAuthenticate(string userName, string password);
        bool ChangePassword(int id, string oldPassword, string newPassword);
        UserDto SignIn(string userName, string password, string email);
        List<UserDto> GetAllUsers();

    }


}