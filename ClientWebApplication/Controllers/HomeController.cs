﻿using System;
using System.Web.Mvc;
using ClientWebApplication.Models;
using ClientWebApplication.Service;

namespace ClientWebApplication.Controllers{
    
    public class HomeController : Controller
    {
        private readonly IUserRepository _userService;

        public HomeController(IUserRepository userService)
        {
            _userService = userService;
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }


       /* public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(SignInModel model)
        {
            _userService.SignIn(model.UserName, model.Password, model.Email);
            return View("WellDone");
        }*/
    }
}