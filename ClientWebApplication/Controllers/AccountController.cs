﻿using System.Web.Mvc;
using ClientWebApplication.Models;
using ClientWebApplication.Service;


namespace ClientWebApplication.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserRepository _userService;

        public AccountController(IUserRepository repository)
        {
            _userService = repository;
        }

        public ActionResult ChangePassword()
        {
            return View();
        }
      
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            var id = (int) Session["userId"];
            var result = _userService.ChangePassword(id, model.Password, model.NewPassword);
           
            return result ? View("WellDone") : View("ChangePassword");
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            var isAuth = _userService.IsAuthenticate(model.UserName, model.Password);
            Session["userId"] = isAuth.UserId;
            Session["userName"] = isAuth.UserName;
            Session["userName"] = isAuth.IsAuth;

            return RedirectToAction(isAuth.IsAuth ? "ChangePassword" : "SignIn", new {area = ""});
        }


        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(SignInModel model)
        {
            var user = _userService.SignIn(model.UserName, model.Password, model.Email);
            Session["userId"] = user.Id;
            Session["userName"] = user.Name;

            return View("WellDone");
        }
    }
}