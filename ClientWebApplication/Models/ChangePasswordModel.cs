﻿using System.ComponentModel.DataAnnotations;

namespace ClientWebApplication.Models
{
    public class ChangePasswordModel
    {
      
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Old Password")]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; } 
    }
}