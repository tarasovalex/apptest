﻿using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WcfService1.dao;

namespace WcfTestProject
{
    /**
     * Tests should be running on a clean database
     */
    [TestClass]
    public class UnitTest1
    {
        private const string TestName = "TestUser";
        private const string TestPassword = "TestPassword";
        private const string TestMail = "est@testmail.com";
        private const int UsersCountTest = 10;

        [TestMethod]
        [TestCleanup]
        [TestInitialize]
        public void DeleteAllUsersTest()
        {
            var userDao = new UserDao();
            var users = userDao.GetAllUsers();

            foreach (var user in users)
            {
                userDao.DeleteUser(user.Id);
            }

            Assert.AreEqual(userDao.GetAllUsers().Count, 0);
        }

        [TestMethod]
        public void AddUserTest()
        {
            var userDao = new UserDao();
            var expacted = userDao.AddUser(TestName, TestPassword, TestMail);
            var actual = userDao.GetUser(expacted.Id);
            
            Assert.AreEqual(TestName, actual.UserName);
            Assert.AreEqual(TestPassword, actual.Password);
            Assert.AreEqual(TestMail, actual.Email);

            userDao.DeleteUser(actual.Id);
        }

        [TestMethod]
        public void DeleteUserTest()
        {
            var userDao = new UserDao();

            var expacted = userDao.AddUser(TestName, TestPassword, TestMail);
            userDao.DeleteUser(expacted.Id);
            var user = userDao.GetUser(expacted.Id);
            Assert.IsNull(user);
        }

        [TestMethod]
        public void ChangePasswordTest()
        {
            var userDao = new UserDao();
            var expacted = userDao.AddUser(TestName, TestPassword, TestMail);
            userDao.ChangePassword(expacted.Id, TestPassword, "NewPassword");
            var actual = userDao.GetUser(expacted.Id);
            Assert.AreEqual(actual.Password, "NewPassword");

            userDao.DeleteUser(expacted.Id);
        }

       
        [TestMethod]
        public void GetAllUsersTest()
        {
            var userDao = new UserDao();
            for (var i = 0; i < UsersCountTest; i++)
            {
                userDao.AddUser(TestName, TestPassword, TestMail);
            }

            Assert.AreEqual(userDao.GetAllUsers().Count, UsersCountTest);

        }
    }
}
