﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WcfService1.model;

namespace WcfService1.dao
{
    public class UserDao : IUserDao
    {
        public AuthDto IsAuthenticate(string userName, string password)
        {
            var user = GetUser(userName);

            return user == null ? new AuthDto(-1, String.Empty, false) : new AuthDto(user.Id, user.UserName, true);
        }

        public bool ChangePassword(int id, string oldPassword, string newPassword)
        {
            var user = GetUser(id);
            if (user == null) return false;
            using (var ctx = new AppUserDbContext())
            {
                user.Password = newPassword;
                ctx.Entry(user).State = EntityState.Modified;
                ctx.SaveChanges();
            }

            return true;
        }

        public User AddUser(string userName, string password, string email)
        {
            var user = new User()
            {
                Password = password,
                UserName = userName,
                Email = email
            };

            using (var ctx = new AppUserDbContext())
            {
                ctx.Users.Add(user);

                ctx.SaveChanges();
            }

            return user;
        }

        public List<User> GetAllUsers()
        {
            using (var ctx = new AppUserDbContext())
            {
                return ctx.Users.ToList();
            }
        }

        public User GetUser(string userName)
        {
            using (var ctx = new AppUserDbContext())
            {
                return ctx.Users.FirstOrDefault(x => x.UserName == userName);
            }
        }

        public User GetUser(int id)
        {
            using (var ctx = new AppUserDbContext())
            {
                return ctx.Users.FirstOrDefault(x => x.Id == id);
            }
        }

        public User DeleteUser(int id)
        {
            using (var ctx = new AppUserDbContext())
            {
                var user = GetUser(id);
                ctx.Users.Attach(user);
                ctx.Users.Remove(user);
                ctx.SaveChanges();

                return user;
            }
        }

        public void DeleteAllUsers()
        {
            throw new NotImplementedException();
        }
    }
}