﻿using System.Collections.Generic;
using WcfService1.model;

namespace WcfService1.dao
{
    public interface IUserDao
    {
        AuthDto IsAuthenticate(string userName, string password);
        bool ChangePassword(int id, string oldPassword, string newPassword);
        List<User> GetAllUsers();
        User GetUser(string userName);
        User GetUser(int id);
        User AddUser(string userName, string password, string email);
        User DeleteUser(int id);
        void DeleteAllUsers();
    }

    public struct AuthDto
    {
        public int UserId { get; set; }
        public bool IsAuth { get; set; }
        public string UserName { get; set; }

        public AuthDto(int userId, string userName, bool isAuth)
            : this()
        {
            UserId = userId;
            IsAuth = isAuth;
            UserName = userName;
        }
    }
}
