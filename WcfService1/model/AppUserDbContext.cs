using System.Data.Entity;

namespace WcfService1.model
{
    public class AppUserDbContext : DbContext
    {
        public AppUserDbContext()
            : base("name=AppUserDbContext")
        {
        }

        public DbSet<User> Users { get; set; }

    }
}