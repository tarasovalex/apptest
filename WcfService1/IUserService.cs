﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfService1.dao;
using WcfService1.model;

namespace WcfService1
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        AuthDto Authenticate(string userName, string userPassword);

        [OperationContract]
        bool ChangePassword(int id, string oldPassword, string newPassword);

        [OperationContract]
        User AddUser(string userName, string password, string email);

        [OperationContract]
        List<User> GetAllUsers();
    }
   
}
