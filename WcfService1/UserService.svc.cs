﻿using System.Collections.Generic;
using WcfService1.dao;
using WcfService1.model;


namespace WcfService1
{
    public class Service1 : IUserService
    {
        private readonly IUserDao _userDao = new UserDao();

        public AuthDto Authenticate(string userName, string userPassword)
        {
            return _userDao.IsAuthenticate(userName, userPassword);
        }

        public bool ChangePassword(int id, string oldPassword, string newPassword)
        {
            return _userDao.ChangePassword(id, oldPassword, newPassword);
        }

        public User AddUser(string userName, string password, string email)
        {
            return _userDao.AddUser(userName, password, email);
        }

        public List<User> GetAllUsers()
        {
            return _userDao.GetAllUsers();
        }
    }
}
